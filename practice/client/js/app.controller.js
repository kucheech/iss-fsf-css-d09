
(function () {
    "use strict";
    angular.module("MyApp").controller("MyAppCtrl", MyAppCtrl);

    MyAppCtrl.$inject = ["$http"];

    function MyAppCtrl($http) {
        var self = this; // vm

        self.completedRegistration = false;
        self.countries = [];
        self.user = {
            email: "",
            password: "",
            gender: "",
            dob: "",
            address: ""
        };

        self.initForm = function () {
            self.completedRegistration = false;

            $http.get("/hello")
                .then(function (result) {
                    console.log(result);
                    self.reply = result.data;
                }).catch(function (e) {
                    console.log(e);
                });

            $http.get("/countries")
                .then(function (result) {
                    console.log(result);
                    self.countries = result.data;
                }).catch(function (e) {
                    console.log(e);
                });
        };

        self.initForm();

        self.isAbove18 = function () {
            if (self.user.dob == "") {
                return false;
            }

            return _calculateAge(self.user.dob) > 18;
        }

        self.submitForm = function () {
            self.completedRegistration = true;
            console.log(self.user.country);

            $http.post("/register", self.user)
                .then(function (result) {
                    console.log(result);
                }).catch(function (e) {
                    console.log(e);
                });
        }
    }

    function _calculateAge(birthday) { // birthday is a date
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }
})();